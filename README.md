Tile server import role
=========

A role to import OSM data into tile server.

Requirements
------------

This role assume a tile server has been provisioned by using [tile-server-role](https://gitlab.com/champs-libres/public/tile-servers/tile-server-role).

Role Variables
--------------

See [the defaults variables](defaults/main.yml), which are documented.

Example Playbook
----------------

This repository contains an example of file for starting a server from openstack, 
install tile server, import data and generate tiles.

See [the example file](example-update-from-openstack.yml).

License
-------

MIT

Author Information
------------------

- Jonathan Beliën (TODO: add an email ?) ;
- https://www.champs-libres.coop, info@champs-libres.coop ;




